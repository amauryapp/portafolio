<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/[{name}]', function (Request $request, Response $response, array $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");

    // Render index view
    return $this->renderer->render($response, 'index.phtml', $args);
});


$app->group('/v1', function () use ($app) {
    $app->group('/user', function () use ($app) 
    {
	    $app->get('/','obtenerUsuarios');
	    $app->get('/{id}', 'obtenerUsuario');
	    $app->get('/search/{id}', 'getSearchUsuario');
	    $app->post('/save', 'agregarUsuario');
	    $app->put('/update/{id}', 'actualizarUsuario');
	    $app->delete('/del/{id}', 'eliminarUsuario');
	});

});
