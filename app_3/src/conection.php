<?php
function getConnection() {
    $dbhost="localhost:3307";
    $dbuser="root";
    $dbpass="";
    $dbname="demo";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}
?>