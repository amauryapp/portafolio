<?php


function obtenerUsuarios($response,$request) {
    
    //echo var_dump($headerValueArray);
    $sql = "SELECT * FROM usuarios";
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        
        return json_encode($employees);
    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function obtenerUsuario($request) {
	$id = $request->getAttribute('id');
    $sql = "SELECT * FROM usuarios WHERE id_user=$id";
    //echo $sql;
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        
        return json_encode($employees);
    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function getSearchUsuario($request) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM usuarios WHERE email=$id";
    //echo $sql;
    try {
        $stmt = getConnection()->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        
        return json_encode($employees);
    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}


function eliminarUsuario($request) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM usuarios WHERE id_user=$id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id_user", $id);
        $stmt->execute();
        $db = null;
        echo '{"error":{"text":"se elimino el usuario"}}';
    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}

function agregarUsuario($request) {
    $user = json_decode($request->getBody());
    
    $sql = "INSERT INTO usuarios (nomb_user, ape_user, ident_user, login_user,paswd_user,email) 
    VALUES (:nomb_user, :ape_user, :ident_user, :login_user,:paswd_user,:email)";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $pass =password_hash($user->paswd_user, PASSWORD_DEFAULT);
        //echo $user->mail_user_fact;
        $stmt->bindParam("nomb_user", $user->nomb_user);
        $stmt->bindParam("ape_user", $user->ape_user);
        $stmt->bindParam("ident_user", $user->ident_user);
        $stmt->bindParam("login_user", $user->login_user);
        $stmt->bindParam("paswd_user", $pass);
        $stmt->bindParam("email", $user->email);
        $stmt->execute();
        $emp->id = $db->lastInsertId();
        $db = null;
        echo json_encode($user);
    } 
    catch(PDOException $e) 
    {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
  
?>