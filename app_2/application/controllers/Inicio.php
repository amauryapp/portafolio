<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
	{   
	    parent::__construct();
	    // Carga modelos para acceso a BD 
	    $this->load->model('Usuarios_Model');
	}

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function register(){
		// Obtener valores del formulario
		$nombre 		= ucwords(trim($this->input->post('nomb_user')));
		$apellido 		= ucwords(trim($this->input->post('ape_user')));
		$identificacion = trim($this->input->post('ident_user'));
		$login_user		= strtolower($this->input->post('login_user'));
		$pass1 			= trim($this->input->post('password'));
		$pass2 			= trim($this->input->post('password_again'));
		$email  		= strtolower($this->input->post('email'));

		// Guardo los valores cargados para recargarlos en caso de error de validación
		$valores = $this->input->post();
		foreach($valores as $clave=>$valor)
		{
			$this->session->set_flashdata($clave,$valor);
		}

		// Validar el Correo Electrónico
		if(!(filter_var($email, FILTER_VALIDATE_EMAIL))) {
        	$this->session->set_flashdata('registro_incorrecto','Escriba una dirección de correo electrónico válida');
           	redirect('login');
		}

		// Valida que las contraseñas coincidan
		if(!($pass1===$pass2)) 
		{
        	$this->session->set_flashdata('registro_incorrecto','Las contraseñas no coinciden');
           	redirect('login');			
		}

		// Verificar que el correo no este ya registrado
		$existe_email = $this->Usuarios_Model->getUserWithEmail($email);
		if($existe_email) 
		{
        	$this->session->set_flashdata('usuario_incorrecto','Su dirección de correo ya esta registrada en el sistema<br> Inicie sesión o recupere su contraseña');
           	redirect('login');			
		}

		$data_user['ident_user'] 	    = $identificacion;
		$data_user['nomb_user'] 		= $nombre;
		$data_user['ape_user'] 		    = $apellido;
		$data_user['login_user']		= $login_user;
		$data_user['paswd_user']		= password_hash($pass1,PASSWORD_DEFAULT);

		$user = $this->Usuarios_Model->save($data_user);
		$registro = FALSE;
		
		if($registro) {
        	$this->session->set_flashdata('registro_completo','Gracias por registrarse, inicie sesión con los datos ingresados');
           	redirect(base_url('inicio'));	
		} else {
        	$this->session->set_flashdata('registro_incorrecto','No fue posible realizar su registro, por favor intente más tarde');
           	redirect(base_url('inicio'));			
		}

	}

}