<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_Model extends CI_Model {

	function __construct()
	{
		parent::__construct();
	}

    function find($id){
        $this->db->where('id_user',$id);
        return $this->db->get('usuarios')->row();        
    }

	function getUser($user) {

		$user = strtolower($user);
		$this->db->where('login_user',$user);
		return $this->db->get('usuarios')->row();
	
	}

	function getUserWithEmail($email) {
		$email = strtolower($email);
		$this->db->where('login_user',$email);
		return $this->db->get('usuarios')->row();
	}

    function save($data){
        $this->db->insert('usuarios', $data);
        $rows = $this->db->affected_rows();
        if($rows>0) {
        	return TRUE;
        } else {
        	return FALSE;
        }
    }

    function update($data,$id){
        $this->db->where('id_user',$id);
        $this->db->update('usuarios', $data);
        $rows = $this->db->affected_rows();
        if($rows>0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function updatePass($data,$email) {
        $this->db->where('login_user', $email);
        $rows = $this->db->update('usuarios', $data);
        if($rows>0) {
        	return TRUE;
        } else {
        	return FALSE;
        }
    }

    function updatePassWithId($pass,$id) {
        $this->db->set('paswd_user',$pass);
        $this->db->where('id_user', $id);
        $rows = $this->db->update('usuarios');
        if($rows>0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function destroy($id){
        $this->db->where('id_user',$id);
        return $this->db->delete('usuarios');
    }


}