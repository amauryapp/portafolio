Portafolio de Desarrollo.
==========

Este es el README de donde se donde se describen las aplicacion realizadas con las tecnologias laboradas y estudiadas.

Aplicaciones
--------------------

+ App_1 : Pagina del Portafolio de desarrollador.<br>
+ App_2 : Aplicacion basada en el framework codeigniter 3, mysql, jquery y boostrap.<br>
+ App_3 : Rest Api para la con Slim Framework 3 (Api de aplicacion de usuarios).<br>
+ App_4 : Pagina Web Construida con VueJs
